# Setup
source("z. boot/startup.R")

fields <- c("taskID", "h1", "h2", "h3", "h4", "h5", 
            "allowed_start", 
            "due_ext_date", "due_ext_time", "due_int_date", 
            "due_int_time", "due_rec_date", "due_rec_time", 
            "duration_exp", "duration_act", "duration_rem",
            "completed")

get_new_Event <- function() {
  
  if (exists("Log")) {
    eventID  <- max(pull(Log, eventID)) + 1
  } else {
    eventID  <- 1
  }
  
  newEvent <<- list(taskID = NA, eventID = eventID, start = NA, end = NA)
  
}
get_new_Task  <- function() {
  
  if (exists("tasks")) {
    taskID  <- max(pull(tasks, taskID)) + 1
  } else {
    taskID  <- 1
  }
  
  newTask$taskID        <<- taskID
  newTask$allowed_start <<- now()
  newTask$due_rec_date  <<- if(is.null(newTask$due_ext_date)) newTask$due_int_date else newTask$due_ext_date
  newTask$due_rec_time  <<- if(is.null(newTask$due_ext_time)) newTask$due_int_time else newTask$due_ext_time
  
  newTask$duration_act        <<- NA
  newTask$duration_rem        <<- NA
  newTask$completed           <<- NA
  
}


shinyServer(function(input, output) {

  # Gantt
  output$timeline <- renderTimevis({
    timevis(data)
  })
  

  
  # Log
  logFormData <- reactive({
    newEvent$taskID <<- input[["taskID"]]
  })
  
  saveStart  <- function() {
    get_new_Event()
    logFormData()
    newEvent$start <<- now()
  }
  saveEnd    <- function(time) {
    newEvent$end <<- now()
  }
  observeEvent(input$start, {
    saveStart()
  })
  observeEvent(input$end, {
    saveEnd()
  })
  loadLog    <- function() {
    
    if (exists("Log")) {
      
      if (exists("newEvent")) {
        
        if (is.na(newEvent$end)) {
          
          rbind(Log, data.frame(newEvent)) %>% mutate(start = as.character(start),
                                                      end   = as.character(end))
          
        } else {
          
          Log <<- rbind(Log, data.frame(newEvent))
          Log %>% mutate(start = as.character(start),
                                end = as.character(end))
          # remove(newEvent)
          
        }
        
      }
      
    } else if (exists("newEvent")) {
      
      if (is.na(newEvent$end)) {
        
        data.frame(newEvent) %>% mutate(start = as.character(start),
                                        end = as.character(end))
        
      } else {
        
        Log <<- data.frame(newEvent)
        Log %>% mutate(start = as.character(start),
                       end = as.character(end))
        # remove(newEvent)
        
      }
      
    }
    
  }
  output$Log <- DT::renderDataTable({
    input$start
    input$end
    loadLog()
  })
  
  
  # Tasks
  
  # Whenever a field is filled, aggregate all form data
  # formData is a reactive function
  taskFormData <- reactive({
    newTask <<- sapply(fields, function(x) input[[x]])
  })
  
  saveTask <- function() {
    get_new_Task()
  }

  # When the Save button is clicked, save the form data
  observeEvent(input$saveTask, {
    taskFormData()
    saveTask()
  })

  
  loadTasks <- function() {
    
    if (exists("tasks")) {
      
      if (exists("newTask")) {
        
        tasks <<- rbind(tasks, data.frame(newTask))
        # remove(newTask)
        
      }
      
      tasks
      
    } else {
      
      if (exists("newTask")) {
        
        assign("tasks", data.frame(newTask), envir = globalenv())
        # tasks <<- data.frame(newTask)
        # remove(newTask)
        tasks
        
      }
      
    }
    
  }
  
  output$tasks <- DT::renderDataTable({
    input$saveTask
    loadTasks()
  })
  

})




data <- data.frame(
  id      = 1:4,
  content = c("Item one", "Item two",
              "Ranged item", "Item four"),
  start   = c("2016-01-10", "2016-01-11",
              "2016-01-20", "2016-02-14 15:00:00"),
  end     = c(NA, NA, "2016-02-04", NA)
)

